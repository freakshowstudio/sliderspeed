About
=====

A simple Unity project demonstrating how the speed of an object can be set 
with a slider (or value otherwise obtained) using the physics engine and
Rigidbody.AddForce.

The movement is dampened with a PD controller (similar to a PID controller
but without the integral factor) for smooth movement.


License
=======

This software is released under the [UNLICENSE](http://unlicense.org/)
license, see the file UNLICENSE for more information.
