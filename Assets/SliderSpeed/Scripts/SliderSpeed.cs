﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controls the speed of a rigidbody in the forward direction
/// using a set value, controlled by a slider.
///
/// Speed uses acceleration and decelleration, dampened
/// by a PD controller.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class SliderSpeed : MonoBehaviour
{
    #region Inspector Variables
    [Header("Movement Properties")]

    [Tooltip("The acceleration of the object")]
    [SerializeField] private float _acceleration = 2f;

    [Tooltip("The decelleration of the object")]
    [SerializeField] private float _decelleration = 3f;

    [Tooltip("The maximum speed of the object")]
    [SerializeField] private float _maxSpeed = 5f;


    [Header("Damping Parameters")]

    [Tooltip("The constant factor of the damping")]
    [SerializeField] private float _pk = 0.5f;

    [Tooltip("The differential factor of the damping")]
    [SerializeField] private float _pd = 0.02f;
    #endregion // Inspector Variables

    #region Private Variables
    private Rigidbody _rigidbody;
    private float _sliderValue = 0f;
    private float _currentSpeed = 0f;
    private float _targetSpeed = 0f;
    private float _deltaSpeed = 0f;
    #endregion Private Variables

    #region Unity Methods
    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        // Get the speed in the forward direction
        _currentSpeed = _rigidbody.velocity.magnitude *
            Vector3.Dot(_rigidbody.velocity, transform.forward);

        _targetSpeed = _sliderValue * _maxSpeed;

        float lastDelta = _deltaSpeed;

        _deltaSpeed = _targetSpeed - _currentSpeed;

        float p =
            (_pk * _deltaSpeed) +
            (_pd * ((_deltaSpeed - lastDelta) / Time.deltaTime));

        if (_deltaSpeed > 0f)
        {
            // Accelerate
            _rigidbody.AddForce(transform.forward * p * _acceleration,
                ForceMode.Acceleration);
        }
        else if (_deltaSpeed < 0f)
        {
            // Decellerate
            _rigidbody.AddForce(transform.forward * p * _decelleration,
                ForceMode.Acceleration);
        }
    }

    void OnGUI()
    {
        Rect sliderRect = new Rect(0f, 0f, Screen.width, 20f);

        _sliderValue = GUI.HorizontalSlider(sliderRect, _sliderValue, 0f, 1f);

        string debugInfo = string.Format(
            "Current Speed: {0} Target Speed: {1} Delta: {2}",
            _currentSpeed, _targetSpeed, _deltaSpeed);

        Rect debugRect = new Rect(0f, 20f, Screen.width, 50f);

        GUI.Label(debugRect, debugInfo);
    }
    #endregion // Unity Methods
}
